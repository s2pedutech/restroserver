var app = require('express').Router();
var restro = require('./restrorent');
app.get('/', restro.getAll);
app.post('/add', restro.add);
module.exports = app;