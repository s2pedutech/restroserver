var db = require('../db/db');

var restroObj = {
    getAll : function(req,res){
        let q = "select * from restaurant";
        db.query(q, function(err,rows){
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    },
    add : function(req,res){
        let restro = req.body;
        console.log(restro);
        let q = "insert into restaurant (name,city,mobile,email, password ) values (?,?,?,?,?)";
        console.log(q);
        db.query(q,[restro.name, restro.city,restro.mobile, restro.email, restro.password], function(err,rows){
            if(err)
            {
                res.send(err);
            }
            else
            {
                res.send(rows);
            }
        });
    }

};
module.exports = restroObj;