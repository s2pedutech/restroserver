var app = require('express').Router();
var customerroutes = require('../customer/customer-routes');
var restroroutes = require('../restrorent/restrorent-routes');
app.use('/customer', customerroutes);
app.use('/restaurant', restroroutes);
module.exports = app;