
var db = require('../db/db');

var customerObj = {
    getAll: function(req,res){
        let q = "select * from customer";
        db.query(q,function(err,rows){
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    },
    add: function(req,res){
        let c = req.body;
        let q = "insert into customer (name,mob,email,city, password) values (?,?,?,?,?)";
        db.query(q, [c.name, c.mob, c.email, c.city, c.password], function(err,rows){
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
        },
    getByEmail: function(req,res){
        console.log(req.query);
        let email = req.query.email;
        console.log(email);
        let q = "Select * from customer where email = ?";
        console.log(q);
        db.query(q, [email], function(err,rows){
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    }
};

module.exports = customerObj;