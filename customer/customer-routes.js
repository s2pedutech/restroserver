var customer = require('./customer');
var app = require('express').Router();
app.get('/', customer.getAll);
app.get('/getByEmail', customer.getByEmail);
app.post('/add', customer.add);
module.exports = app;