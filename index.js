var app = require('express')();
var bodyparser = require('body-parser');
var cors = require('cors');
var routes = require('./routes/routes');

app.use(cors());
app.options('*',cors());
app.use(bodyparser.json());
app.use('/',routes);

app.listen('3000', function(){
    console.log("Listening on 3000");
});